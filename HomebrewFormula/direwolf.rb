# frozen_string_literal: true

class Direwolf < Formula
  desc "Decoded Information from Radio Emissions for Windows Or Linux Fans"
  homepage "https://github.com/wb2osz/direwolf"
  url "https://github.com/wb2osz/direwolf/archive/1.6.tar.gz"
  sha256 "208b0563c9b339cbeb0e1feb52dc18ae38295c40c0009d6381fc4acb68fdf660"
  license "GPL-2.0-only"
  head "https://github.com/wb2osz/direwolf/archive/master.zip"

  depends_on "cmake" => :build
  depends_on "portaudio" => :build
  depends_on "hamlib" => :optional

  def install
    system "cmake", ".", *std_cmake_args
    system "make", "install"
    system "make", "install-conf"
  end

  test do
    system "#{bin}/direwolf", "-u"
  end
end
